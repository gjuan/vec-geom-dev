This library is free software; you can redistribute it and/or
modify it under the terms of the Apache License, Version 2.0
as published by the Apache Software Foundation; either
version 2.0 of the License, or (at your option) any later version.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

You should have received a copy of the Apache License along with this
library; if not, write to the The Apache Software Foundation
401 Edgewater Place, Suite 600, Wakefield, MA 01880 U.S.A.

See APACHE-LICENSE-2.0.txt next to this file or 
   http://www.apache.org/licenses/LICENSE-2.0
for the text of the Apache License, Version 2.0.

---

# Additional licesnses

VecGeom includes excerpts of other codes with the following licenses.

## robin_hood.h

https://github.com/martinus/robin-hood-hashing

> Licensed under the MIT License <http://opensource.org/licenses/MIT>.
> SPDX-License-Identifier: MIT
> Copyright (c) 2018-2019 Martin Ankerl <http://martin.ankerl.com>
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

## Celeritas utilities

https://github.com/celeritas-project/celeritas

> Copyright 2020-2023 UT-Battelle, LLC, and other Celeritas developers.
> SPDX-License-Identifier: (Apache-2.0 OR MIT)
>
> Celeritas is licensed under the Apache License, Version 2.0 (LICENSE-APACHE
> or http://www.apache.org/licenses/LICENSE-2.0) or the MIT license,
> (LICENSE-MIT or http://opensource.org/licenses/MIT), at your option.
> 
> Copyrights and patents in the Celeritas project are retained by contributors.
> No copyright assignment is required to contribute to Celeritas.
